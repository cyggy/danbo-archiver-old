#if !defined(FCURL_H)
#define FCURL_H

#include <stdlib.h>

struct string {
  char* str;
  size_t size;
};

static size_t write_json(void *contents, size_t size, size_t nmemb, void *userp);
void get_threadlist(char* b, struct string* ret);
void get_thread(char* b, char* no, struct string* ret);

#endif
