danbo.x: danbo.o 4curl.o db.o json_handler.o
	gcc danbo.o 4curl.o db.o json_handler.o -o danbo.x -lcurl -ljson-c
json_handler.o: json_handler.h
	gcc json_handler.h -c json_handler.c
db.o: db.h
	gcc db.h -c db.c
4curl.o: 4curl.h
	gcc 4curl.h -c 4curl.c
danbo.o: 4curl.h db.h
	gcc -c danbo.c
clean:
	rm -f *.o *.gch danbo.x
