#include <stdio.h>
#include <json-c/json.h>
#include "4curl.h"

void parse_threadlist(struct string* ret_tl) {
  json_object* jobj;
  jobj = json_tokener_parse(ret_tl->str);
  //printf("jobj from str:\n---\n%s\n---\n", json_object_to_json_string_ext(jobj, JSON_C_TO_STRING_SPACED | JSON_C_TO_STRING_PRETTY));

  /*4chan api is parsed as array of size 10. Each index contains an another json
    object containing keys "page" and "threads". "threads" is another array of
    objects containing arrays that carry 2 pieces of data, the thread number
    and time of last modification. for our purposes we only need these to points
    of data and store them.
   */
  int board_thread_data[310] = { 0 };
  int k = 0;
  array_list* arrl = json_object_get_array(jobj);//get the array list of 10
  enum json_type type;
  //loop through the objects
  for (int i = 0; i < arrl->length; i++) {
    //each object contains the keys "page" and "threads"
    json_object_object_foreach(arrl->array[i], key, val) {      
      switch (json_object_get_type(val)) {
	//we ignore the "page" key, only care about the "threads" key
	//which is an array of objects again
      case json_type_array: ;
	//grab the objects into an arraylist
	array_list* tmp = json_object_get_array(val);
	for(int j = 0; j < tmp->length; j++) {	  
	  json_object_object_foreach(tmp->array[j], key, val) {
	    //each object contains the thread number and last modification time
	    board_thread_data[k++] = json_object_get_int(val);
	  }
	}
	break;
      }
    }
  }

  for(int i = 0; i < 310; i++){
    if (board_thread_data[i] == 0) break;
    printf("%d\n", board_thread_data[i]);
  }
}
