#include "4curl.h"
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>

static size_t write_json(void *contents, size_t size, size_t nmemb, void *userp) {
  size_t realsize = size * nmemb;
  struct string* mem = (struct string*)userp;
  
  mem->str = realloc(mem->str, mem->size + realsize + 1);
  
  if(mem->str == NULL) {
    /* out of memory! */
    printf("not enough memory (realloc returned NULL)\n");
    return 0;
  }
  
  memcpy(&(mem->str[mem->size]), contents, realsize);
  mem->size += realsize;
  mem->str[mem->size] = 0;
  
  return realsize;
}

void get_threadlist(char* b, struct string* ret) {
  CURL* curl;
  CURLcode res;
  char* json_link = malloc(sizeof(char) * 36);

  if (json_link == NULL) {
    fprintf(stderr, "malloc() failed\n");
    exit(EXIT_FAILURE);
  }
    
  strcpy(json_link, "http://a.4cdn.org/");
  strcat(json_link, b);
  strcat(json_link, "/threads.json");

  curl = curl_easy_init();

  if (curl) {
    curl_easy_setopt(curl, CURLOPT_URL, json_link);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_json);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)ret);
    res = curl_easy_perform(curl);
    
    if (res != CURLE_OK)
      fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));

    curl_easy_cleanup(curl);
  }
  
  free(json_link);
}

void get_thread(char* b, char* no, struct string* ret) {
  CURL* curl;
  CURLcode res;
  char* json_link = malloc(sizeof(char) * 49);

  if (json_link == NULL) {
    fprintf(stderr, "malloc() failed\n");
    exit(EXIT_FAILURE);
  }
    
  strcpy(json_link, "http://a.4cdn.org/");
  strcat(json_link, b);
  strcat(json_link, "/thread/");
  strcat(json_link, no);
  strcat(json_link, ".json");

  curl = curl_easy_init();

  if (curl) {
    curl_easy_setopt(curl, CURLOPT_URL, json_link);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_json);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)ret);
    curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1);
    res = curl_easy_perform(curl);
    
    if (res != CURLE_OK)
      fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
    
    curl_easy_cleanup(curl);
  }

  free(json_link);
}

