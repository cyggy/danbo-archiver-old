#include <stdio.h>
#include <string.h>
#include "4curl.h"
#include "json_handler.h"
#include "db.h"

int main() {
  char* board = "g\0";
  char* no = "0\0";
  struct string ret_tl;
  struct string ret_no;

  ret_tl.str = malloc(1);
  ret_tl.size = 0;

  ret_no.str = malloc(1);
  ret_no.size = 0;

  if (ret_tl.str == NULL || ret_no.str == NULL) {
    fprintf(stderr, "malloc failed()\n");
    exit(EXIT_FAILURE);
  }

  get_threadlist(board, &ret_tl);
  get_thread(board, no, &ret_no);

  parse_threadlist(&ret_tl);
  
  free(ret_tl.str);
  free(ret_no.str);

  return 0;
}
